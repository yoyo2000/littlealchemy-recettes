## Recettes pour le site http://littlealchemy.com/

Vous en réviez, voici la liste de toutes les recettes.

## Exemple

Un bouton permet d'afficher le résultat des recettes.
Vous pouvez passer par la recherche pour voir si vous avez toutes les recettes avec un ingrédient.
Le mot clé "caché" permet de trouver les éléments non recensés.
Le mot clé "idem" permet de trouver les recettes où les 2 ingrédients sont identiques.

## License

WTFPL : http://www.wtfpl.net/