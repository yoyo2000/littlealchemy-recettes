'use strict';
var noms;
var recettes;
var imgs;
var aff_imgs = true;
var finaux;

$(document).ready(function() {
	
	$.ajaxSetup({async:false});
	$.getJSON("noms.json", function(data) {
		if ($.isPlainObject(data)){
			noms = data;
		}else{
			alert( "Problème de chargement des noms" );
		}
	})
	.fail(function( jqxhr, textStatus, error ) {
		alert( "Erreur : " + textStatus + ", " + error );
	});
	$.getJSON("recettes.json", function(data) {
		if ($.isPlainObject(data)){
			recettes = data;
		}else{
			alert( "Problème de chargement des recettes" );
		}
	})
	.fail(function( jqxhr, textStatus, error ) {
		alert( "Erreur : " + textStatus + ", " + error );
	});
	$.getJSON("images.json", function(data) {
		if ($.isPlainObject(data)){
			imgs = data;
		}else{
			alert( "Problème de chargement des images" );
		}
	})
	.fail(function( jqxhr, textStatus, error ) {
		alert( "Erreur : " + textStatus + ", " + error );
	});
	$.ajaxSetup({async:true});
	
	finaux = calcul_finaux();
	
	rempli_tab(aff_imgs);
	$('.loader').hide("slow");
	$('#body').show("slow");
	
	$('#btn').click(function(){
		if ($("th:contains('Résultat')").length > 0){
			$('#tab').dataTable().fnSetColumnVis( 2, false );
		}else{
			$('#tab').dataTable().fnSetColumnVis( 2, true );
		}
	});
	
	// $('#btn2').click(function(){
		// $('#tab').DataTable().clear();
		// $('#tab').DataTable().destroy();
		// rempli_tab(aff_imgs);
		// aff_imgs = !aff_imgs;
	// });

	function rempli_tab(affiche_image){
		// Construction du tableau
		var tab = '';
		var num = 1;
		$.each(recettes, function(i,rec){
			$.each(rec.parents, function(j, ing) {
				tab += '<tr>';
				tab += '<td>'+num+'</td>';
				var cach,idem;
				(rec.hidden !== undefined) ? cach="(caché)" : cach="";
				(ing[0] == ing[1]) ? idem="(idem)" : idem="";
				if (affiche_image){
					tab += '<td><div class="combination">'+creer_div(ing[0])+'<div class="plus">+</div>'+creer_div(ing[1])+'<mark>'+idem+'</mark></div></td>';
					tab += '<td><div class="combination"><div class="egal">=</div>'+creer_div(i)+'<mark>'+cach+'</mark></div></td>';
				}else{
					tab += '<td>'+noms[ing[0]]+' + '+noms[ing[1]]+'</td>';
					tab += '<td>'+noms[i]+'</td>';
				}
				tab += '</tr>';
				num++;
			});
		});
		$("#letab").html(tab);
		
		$('#tab').dataTable( {
			"bPaginate": false,
			"order": [],
			"sDom": 'fitfi',
			"oLanguage": {
				"sZeroRecords": "Aucun résultat",
				"sInfo": "Affiche _TOTAL_ enregistrements",
				"sInfoEmtpy": "Aucun résultat",
				"sInfoFiltered": "(filtré à partir de _MAX_ enregistrements)"
			}
		} );
		
		$('#tab').dataTable().fnSetColumnVis( 2, false );
	}	
		
	function creer_div(indice){
		// <div class="elementBox"><img class="elementImg" src="img/base/231.png">mouse</div>
		var fin;
		($.inArray(parseInt(indice), finaux) != -1) ? fin = " final" : fin = "";
		return '<div class="elementBox' + fin + '"><img class="elementImg" src="data:image/png;base64,'+ imgs[indice] +'">'+ noms[indice] +'</div>';
	}
	
	function calcul_finaux(){
		var n = Object.keys(noms).length+1;
		var fin = Array.apply(null, {length: n}).map(Number.call, Number); // [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 587 de plus… ]
		$.each(recettes, function(i,rec){
			$.each(rec.parents, function(j, ing) {
				fin = $.grep(fin, function (val) { return val != ing[0] ; });
				fin = $.grep(fin, function (val) { return val != ing[1] ; });
			});
		});
		return fin;
	}

});